import React, { Suspense } from "react";
import logo from "./logo.svg";
import "./App.css";

import { Trans, withTranslation } from "react-i18next";

function App(props) {
  const { t, i18n } = props;
  const changeLanguage = lng => {
    i18n.changeLanguage(lng);
  };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Trans i18nKey="description.part1" t={t}>
          To get start, edit <h3>{{ name: "src/App.js" }}</h3> and s.
        </Trans>
        <div>{t("description.part2")}</div>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          {t("title")}
        </a>
        <button onClick={() => changeLanguage("de")}>de</button>
        <button onClick={() => changeLanguage("en")}>en</button>
      </header>
    </div>
  );
}

export default withTranslation("common")(App);
