import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import Backend from "i18next-chained-backend";
import LocalStorageBackend from "i18next-localstorage-backend"; // primary use cache
import XHR from "i18next-xhr-backend"; // fallback xhr load
import LanguageDetector from "i18next-browser-languagedetector";

import moment from "moment";

// the translations
// (tip move them in a JSON file and import them)
const resources = {
  en: {
    translation: {
      "Learn React": "Welcome to React and react-i18next {{date,MM-DD-YY}}",
      key: "The current date is {{date,MM-DD-YY}}"
    }
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .use(Backend)
  .use(LanguageDetector)
  .init({
    //resources,
    fallbackLng: "en",
    ns: [],
    defaultNS: false,
    debug: true,
    // saveMissing: true,
    backend: {
      backends: [
        //LocalStorageBackend, // primary
        XHR // fallback
      ],
      backendOptions: [
        {
          // prefix: "i18next_res_",
          // expirationTime: 7 * 24 * 60 * 60 * 1000,
          // store: window.localStorage
        },
        {
          loadPath: "/locales/{{lng}}/{{ns}}.json",
          // loadPath:
          //   "https://raw.githubusercontent.com/i18next/i18next-gitbook/master/locales/{{lng}}/{{ns}}.json",
          crossDomain: true
          // addPath:
          //   "https://raw.githubusercontent.com/i18next/i18next-gitbook/master/add/{{lng}}/{{ns}}"
        }
      ]
    },
    // keySeparator: true, // we do not use keys in form messages.welcome
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;
